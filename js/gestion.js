var url="https://services.odata.org/V4/Northwind/Northwind.svc";
var productosObtenidos;
var clientesObtenidos;

function getProductos() {
  var products = url + "/Products"
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200)
      productosObtenidos = request.responseText;
      procesarObtenidos();
  };
  request.open("GET",products,true);
  request.send();
}

function getCustomers() {
  var customers = url + "/Customers"
  var filteredCustomers = customers; //+ "?$filter=Country eq 'Germany'";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200)
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
  };
  request.open("GET",filteredCustomers);
  request.send();
}

function procesarObtenidos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //alert(JSONProductos.value[0].ProductName);
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;

    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  //https://www.countries-ofthe-world.com/flags-normal/flag-of-Uruguay.png
  //alert(JSONClientes.value[0].ProductName);
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].ProductName
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country=="UK"){
      imgBandera.src = rutaBandera+"United-Kingdom.png"
    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }

    var columnaBandera = document.createElement("td");
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);
    //nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

// Entrega SDLC - Proyecto
